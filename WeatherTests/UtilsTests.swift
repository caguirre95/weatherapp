//
//  UtilsTests.swift
//  WeatherTests
//
//  Created by Cristhian Antonio Aguirre Corea on 6/5/22.
//

import XCTest

class UtilsTests: XCTestCase {
    
    //TESTS FOR DEVICE UTILS
    func testGetDeviceLocation()  {
        let expected = "ni"
        let deviceLocation = DeviceUtils.shared.getDeviceLocation()?.lowercased()
        XCTAssert(expected == deviceLocation)
    }
    
    func testGetDeviceLanguage()  {
        let expected = "es"
        let deviceLanguage = DeviceUtils.shared.getDeviceLanguage()
        XCTAssert(expected == deviceLanguage)
    }
    
    //TEST FOR DOUBLE EXT
    func testFormatDoubleExt() {
        let expected = "32.10"
        let valueToTest = 32.10423432.toString(format: "%.2f")
        XCTAssert(expected == valueToTest)
    }
    
    //TEST FOR BUNDLE UTILS
    func testGetDataFrom()  {
        let dataValue = BundleUtils.getDataFrom("WeatherErrorSampleData")
        XCTAssert(!dataValue.isEmpty)
    }
    
    
}
