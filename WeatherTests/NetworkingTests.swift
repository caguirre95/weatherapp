//
//  NetworkingTests.swift
//  WeatherTests
//
//  Created by Cristhian Antonio Aguirre Corea on 5/5/22.
//

import XCTest
@testable import Weather

class NetworkingTests: XCTestCase {
    
    var httpManager: HttpManager? = nil
    
    override func setUp() {
        super.setUp()
        httpManager = HttpManager()
    }
    
    //TESTING ERROR MODEL Y CODIGOS LOCALES
    func testNullableLocalError()  {
        let expectedNilValue: String? = nil
        
        let error = WeatherLocalError.nullable(code: WeatherLocalErrorCodes.nullable.rawValue,
                                               message: NSLocalizedString("nullable response_decodeModel", comment: ""))
        
        switch error {
        case .nullable(let code, _):
            if expectedNilValue == nil {
                XCTAssertTrue(code == WeatherLocalErrorCodes.nullable.rawValue, "Provocar error con nullable code")
            }
        default:
            XCTFail("Test solo para validar nullable")
        }

    }
    
    func testCatcherLocalError()  {
        do {
            try throwTest()
        } catch let error {
            guard let weatherLocalError = error as? WeatherLocalError else {
                XCTFail("Debe de ser modelo de error local")
                return
            }
            
            switch weatherLocalError {
            case .catcher(let code, _):
                XCTAssert( code == WeatherLocalErrorCodes.catcher.rawValue ,
                          "Provocar error con catcher code")
            default:
                XCTFail("Test solo para validar catcher")
            }
    
        }
    }
    
    func testWeatherReponseWithHttpManager()  {
        let lat = "12.145991"
        let long = "-86.274666"
        let deviceLang = "es"
        httpManager?.request(target: .weather(lat: lat,
                                              lon: long,
                                              lang: deviceLang),
                             isForTest: true,
                             success: proccessTestWeatherResponse,
                             failure: proccessTestWeatherError)
    }
    
    func testWeatherErrorResponseWithHttpManager()  {
        let lat = "12.145991"
        let long = "-86.274666"
        let deviceLang = "es"
        httpManager?.request(target: .weather(lat: lat,
                                              lon: long,
                                              lang: deviceLang),
                             isForTest: true,
                             withCustomSampleResponse: true,
                             success: proccessTestWeatherResponse,
                             failure: proccessTestWeatherError)
    }
    
    func testGeoCodeReponseWithHttpManager() {
        let query = "MaNaguA".lowercased()
        httpManager?.request(target: .geoCoding(query: query),
                             isForTest: true,
                             success: proccessTestGeoCodeResponse,
                             failure: proccessTestWeatherError)
        
    }
    
    func testGeoCodeErrorReponseWithHttpManager() {
        let query = "MaNaguA".lowercased()
        httpManager?.request(target: .geoCoding(query: query),
                             isForTest: true,
                             withCustomSampleResponse: true,
                             success: proccessTestGeoCodeResponse,
                             failure: proccessTestWeatherError)
        
    }
    
    func testGeoCodeReverseReponseWithHttpManager() {
        let lat = "12.145991"
        let long = "-86.274666"
        httpManager?.request(target: .reverseGeoCoding(lat: lat, lon: long),
                             isForTest: true,
                             success: proccessTestGeoCodeResponse,
                             failure: proccessTestWeatherError)
        
    }
    
    func testGeoCodeReverseErrorReponseWithHttpManager() {
        let lat = "12.145991"
        let long = "-86.274666"
        httpManager?.request(target: .reverseGeoCoding(lat: lat, lon: long),
                             isForTest: true,
                             withCustomSampleResponse: true,
                             success: proccessTestGeoCodeResponse,
                             failure: proccessTestWeatherError)
        
    }
    
    private func proccessTestGeoCodeResponse(model: GeoCode?) {
        XCTAssertNotNil(model, "Modelo con valor")
    }

    private func proccessTestWeatherResponse(model: WeatherInfo?) {
        XCTAssertNotNil(model, "Modelo con valor")
    }

    private func proccessTestWeatherError(error: Error?) {
        guard let error = error else {
            XCTFail("Error viene nil")
            return
        }
        XCTAssertTrue(error is WeatherError)
    }
    
    private func throwTest() throws {
        let error = WeatherLocalError.catcher(code: WeatherLocalErrorCodes.catcher.rawValue,
                                              message: NSLocalizedString("catch_error", comment: ""))
        throw error
    }

}
