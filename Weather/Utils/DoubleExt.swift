//
//  DoubleExt.swift
//  Weather
//
//  Created by Cristhian Antonio Aguirre Corea on 5/5/22.
//

import Foundation

extension Double {

    func toString(format: String? = nil) -> String {
        if let format = format {
            return String(format: format, self)
        }

        return String(self)
    }

}
