//
//  StringExt.swift
//  Weather
//
//  Created by Cristhian Antonio Aguirre Corea on 7/5/22.
//

import Foundation

extension String {
    func localize() -> String {
        return NSLocalizedString(self, comment: "")
    }
}
