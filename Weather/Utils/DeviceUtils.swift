//
//  DeviceUtils.swift
//  Weather
//
//  Created by Cristhian Antonio Aguirre Corea on 5/5/22.
//

import Foundation

class DeviceUtils {

    static let shared = DeviceUtils()

    private init() { }

    func getDeviceLocation() -> String? {
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            return countryCode.lowercased()
        }

        return nil
    }

    func getDeviceLanguage() -> String? {
        return NSLocale.current.languageCode
    }

}
