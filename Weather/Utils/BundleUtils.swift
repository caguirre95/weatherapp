//
//  BundleUtils.swift
//  Weather
//
//  Created by Cristhian Antonio Aguirre Corea on 6/5/22.
//

import Foundation

class BundleUtils {
    static func getDataFrom(_ resource: String?, ext: String = "json") -> Data {
        guard let resource = resource else {
            return Data()
        }

        guard let url = Bundle.main.url(forResource: resource, withExtension: ext),
            let data = try? Data(contentsOf: url) else {
                return Data()
        }
        return data
    }
}
