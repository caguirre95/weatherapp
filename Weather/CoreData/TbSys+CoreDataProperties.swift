//
//  TbSys+CoreDataProperties.swift
//  Weather
//
//  Created by Cristhian Antonio Aguirre Corea on 9/5/22.
//
//

import Foundation
import CoreData

extension TbSys {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TbSys> {
        return NSFetchRequest<TbSys>(entityName: "TbSys")
    }

    @NSManaged public var country: String?
    @NSManaged public var id: UUID?
    @NSManaged public var weatherInfo: TbWeatherInfo?

}

extension TbSys: Identifiable {

}
