//
//  TbWeatherInfo+CoreDataProperties.swift
//  Weather
//
//  Created by Cristhian Antonio Aguirre Corea on 9/5/22.
//
//

import Foundation
import CoreData

extension TbWeatherInfo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TbWeatherInfo> {
        return NSFetchRequest<TbWeatherInfo>(entityName: "TbWeatherInfo")
    }

    @NSManaged public var id: Int32
    @NSManaged public var name: String?
    @NSManaged public var isFavorite: Bool
    @NSManaged public var main: TbMain?
    @NSManaged public var sys: TbSys?
    @NSManaged public var weather: TbWeather?

}

extension TbWeatherInfo: Identifiable {

}
