//
//  TbGeoCode+CoreDataProperties.swift
//  Weather
//
//  Created by Cristhian Antonio Aguirre Corea on 9/5/22.
//
//

import Foundation
import CoreData

extension TbGeoCode {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TbGeoCode> {
        return NSFetchRequest<TbGeoCode>(entityName: "TbGeoCode")
    }

    @NSManaged public var id: UUID?
    @NSManaged public var name: String?
    @NSManaged public var lat: Double
    @NSManaged public var lon: Double
    @NSManaged public var country: String?
    @NSManaged public var state: String?

}

extension TbGeoCode: Identifiable {

}
