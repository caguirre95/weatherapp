//
//  TbMain+CoreDataProperties.swift
//  Weather
//
//  Created by Cristhian Antonio Aguirre Corea on 9/5/22.
//
//

import Foundation
import CoreData

extension TbMain {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TbMain> {
        return NSFetchRequest<TbMain>(entityName: "TbMain")
    }

    @NSManaged public var temp: Double
    @NSManaged public var tempMin: Double
    @NSManaged public var tempMax: Double
    @NSManaged public var id: UUID?
    @NSManaged public var weatherInfo: TbWeatherInfo?

}

extension TbMain: Identifiable {

}
