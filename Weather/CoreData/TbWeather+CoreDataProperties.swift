//
//  TbWeather+CoreDataProperties.swift
//  Weather
//
//  Created by Cristhian Antonio Aguirre Corea on 9/5/22.
//
//

import Foundation
import CoreData

extension TbWeather {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TbWeather> {
        return NSFetchRequest<TbWeather>(entityName: "TbWeather")
    }

    @NSManaged public var id: Int32
    @NSManaged public var main: String?
    @NSManaged public var weatherDescription: String?
    @NSManaged public var icon: String?
    @NSManaged public var weatherInfo: TbWeatherInfo?

}

extension TbWeather: Identifiable {

}
