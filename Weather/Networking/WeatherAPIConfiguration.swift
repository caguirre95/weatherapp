//
//  BaseConfiguration.swift
//  Weather
//
//  Created by Cristhian Antonio Aguirre Corea on 5/5/22.
//

import Moya

public enum WeatherAPIConfiguration {

    static private let appID = "bf3609275fcd60d53f8c471a914b573f"
    static private let units  = "Metric"

    static func getWeatherIcon(with code: String?) -> URL? {
        return URL(string: "http://openweathermap.org/img/w/\(code ?? "").png")
    }

    case weather(lat: String, lon: String, lang: String)
    case geoCoding(query: String)
    case reverseGeoCoding(lat: String, lon: String)
}

extension WeatherAPIConfiguration: TargetType {
    public var baseURL: URL {
        switch self {
        case .weather:
            return URL(string: "https://api.openweathermap.org/data/2.5")!
        case .geoCoding, .reverseGeoCoding:
            return URL(string: "http://api.openweathermap.org/geo/1.0")!
        }
    }

    public var path: String {
        switch self {
        case .weather: return "/weather"
        case .geoCoding: return "/direct"
        case .reverseGeoCoding: return "/reverse"
        }
    }

    public var method: Moya.Method {
        switch self {
        case .weather, .geoCoding, .reverseGeoCoding: return .get
        }
    }

    public var task: Task {
        switch self {
        case .weather(let lat, let lon, let lang):
            return .requestParameters(parameters: [
            "lat": lat,
            "lon": lon,
            "appid": WeatherAPIConfiguration.appID,
            "lang": lang,
            "units": WeatherAPIConfiguration.units
            ],
            encoding: URLEncoding.queryString)
        case .geoCoding(let query):
            return .requestParameters(parameters: [
            "q": query,
            "appid": WeatherAPIConfiguration.appID
            ],
            encoding: URLEncoding.queryString)
        case .reverseGeoCoding(let lat, let lon):
            return .requestParameters(parameters: [
            "lat": lat,
            "lon": lon,
            "limit": "10",
            "appid": WeatherAPIConfiguration.appID
            ],
            encoding: URLEncoding.queryString)
        }
    }

    public var headers: [String: String]? {
        return ["Content-Type": "application/json"]
    }

    public var validationType: ValidationType {
        return .successCodes
    }

    public var sampleData: Data {
        switch self {
        case .weather:
            return BundleUtils.getDataFrom(SampleIdentifier.weatherSampleData.rawValue)
        case .geoCoding, .reverseGeoCoding:
            return BundleUtils.getDataFrom(SampleIdentifier.geoCodingSampleData.rawValue)
            // return BundleUtils.getDataFrom(SampleIdentifier.emptyGeoCodingSuccessData.rawValue)
        }
    }
}
