//
//  HttpManager.swift
//  Weather
//
//  Created by Cristhian Antonio Aguirre Corea on 5/5/22.
//

import Moya

class HttpManager: NSObject, ObservableObject {

    func request<TModel>(target: WeatherAPIConfiguration,
                         isForTest: Bool = false,
                         withCustomSampleResponse: Bool = false,
                         success: ((TModel?) -> Void)?,
                         failure: ((Error?) -> Void)?) where TModel: Codable {

            let provider = provideEnvironment(isForTest: isForTest,
                                              withCustomSampleResponse: withCustomSampleResponse,
                                              target: target)
            provider.request(target) { [weak self] result in
            switch result {
            case .success(let response): self?.decodeModel(with: response,
                                                           success: success,
                                                           failure: failure)

            case .failure(let error): self?.decodeModel(with: error.response,
                                                        isErrorResponse: true,
                                                        success: success,
                                                        failure: failure)
            }
        }

    }

    func decodeModel<TModel: Codable>(with response: Response?,
                                      isErrorResponse: Bool = false,
                                      success: ((TModel?) -> Void)?,
                                      failure: ((Error?) -> Void)?) {

        guard let data = response?.data else {
            let error = WeatherLocalError.nullable(code: WeatherLocalErrorCodes.nullable.rawValue,
                                                   message: NSLocalizedString("nullable response_decodeModel",
                                                                              comment: ""))
            failure?(error)
            return
        }

        do {
            if !isErrorResponse {
                let model = try JSONDecoder().decode(TModel.self, from: data)
                success?(model)
            } else {
                let errorModel = try JSONDecoder().decode(WeatherError.self, from: data)
                failure?(errorModel)
            }

        } catch let error {
            let localError = WeatherLocalError.catcher(code: WeatherLocalErrorCodes.catcher.rawValue,
                                                   message: NSLocalizedString("nullable \(error.localizedDescription)",
                                                                              comment: ""))
            failure?(localError)
        }

    }

    private func provideEnvironment(isForTest: Bool,
                                    withCustomSampleResponse: Bool,
                                    target: WeatherAPIConfiguration) -> MoyaProvider<WeatherAPIConfiguration> {

        if isForTest {
            if withCustomSampleResponse {
                var fileErrorName: String?
                switch target {
                case .weather, .geoCoding, .reverseGeoCoding:
                    fileErrorName = "WeatherErrorSampleData"
                }
                let customEndpointClosure = { (target: WeatherAPIConfiguration) -> Endpoint in
                    return Endpoint(url: URL(target: target).absoluteString,
                                    sampleResponseClosure: { .networkResponse(400,
                                                                            BundleUtils
                                                                                .getDataFrom(fileErrorName))
                                    },
                                    method: target.method,
                                    task: target.task,
                                    httpHeaderFields: target.headers)
                }
                return MoyaProvider<WeatherAPIConfiguration>
                    .init(endpointClosure: customEndpointClosure,
                          stubClosure: MoyaProvider<WeatherAPIConfiguration>.immediatelyStub)
            }

            return MoyaProvider<WeatherAPIConfiguration>
                .init(stubClosure: MoyaProvider<WeatherAPIConfiguration>.immediatelyStub)

        }

        return MoyaProvider<WeatherAPIConfiguration>()
    }

}
