//
//  WeatherApp.swift
//  Weather
//
//  Created by Cristhian Antonio Aguirre Corea on 5/5/22.
//

import SwiftUI

@main
struct WeatherApp: App {

    @EnvironmentObject var connectivity: Connectivity
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            MainView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
                .environmentObject(Connectivity())
        }
    }
}
