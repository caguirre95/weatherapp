//
//  WeatherItemView.swift
//  Weather
//
//  Created by Cristhian Antonio Aguirre Corea on 8/5/22.
//

import SwiftUI

struct WeatherItemView: View {

    var model: TbWeatherInfo

    let multiplierSpacer: CGFloat = 8

    var body: some View {
        VStack {
            HStack {

                Text(model.sys?.country ?? "Unknown")
                    .padding(multiplierSpacer)
                    .font(.system(size: 14,
                                  weight: .bold,
                                  design: .default))

                Spacer()

                Text("temp \(model.main?.temp.toString(format: "%.2f") ?? "Unknown")")
                    .padding(multiplierSpacer)
                    .font(.system(size: 14,
                                  weight: .bold,
                                  design: .default))

            }

            HStack(alignment: .center) {
                Text(model.weather?.weatherDescription?.capitalized ?? "Unknown")
                    .padding(multiplierSpacer)
                    .font(.system(size: 18,
                                  weight: .bold,
                                  design: .default))
            }

            HStack(alignment: .center) {
                AsyncImage(url: WeatherAPIConfiguration
                            .getWeatherIcon(with: model.weather?.icon)) { phase in
                    switch phase {
                    case .empty:
                        ProgressView()
                    case .success(let image):
                        image.resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(maxWidth: 60, maxHeight: 60)
                    case .failure:
                        Image("image_not_available")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(maxWidth: 60, maxHeight: 60)
                    @unknown default:
                        Image("image_not_available")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(maxWidth: 60, maxHeight: 60)
                    }
                }.padding(multiplierSpacer)
            }

            HStack(alignment: .center) {
                Text("max \(model.main?.tempMax.toString(format: "%.0f") ?? "N/A")")
                    .padding(multiplierSpacer)

                Text("min \(model.main?.tempMin.toString(format: "%.0f") ?? "N/A")")
                    .padding(multiplierSpacer)
            }
        }
    }
}

/*
struct WeatherItemView_Previews: PreviewProvider {
    static var previews: some View {
        WeatherItemView()
    }
}
 */
