//
//  MainView.swift
//  Weather
//
//  Created by Cristhian Antonio Aguirre Corea on 5/5/22.
//

import SwiftUI
import CoreData

struct MainView: View {

    @Environment(\.managedObjectContext) private var viewContext

    @FetchRequest(sortDescriptors: [SortDescriptor(\.isFavorite)],
                  predicate: nil,
                  animation: .default) var weathersInfo: FetchedResults<TbWeatherInfo>

    @EnvironmentObject var connectivity: Connectivity
    @ObservedObject private var viewModel = MainViewModel()

    @State var isActive = false
    @State var refreshFavoriteCities = false
    @State private var showingAlertError = false

    var body: some View {
        NavigationView {
            VStack {
                if viewModel.isLoading || viewModel.userLocation == nil {
                    ProgressView()
                } else {
                    List {
                        Section(header: Text("cities_list")) {
                            ForEach(weathersInfo, id: \.id) { model in
                                if model.isFavorite {
                                    HStack {
                                        WeatherItemView(model: model)
                                        Spacer()
                                        Image(systemName: "star.fill")
                                            .foregroundColor(.yellow)
                                    }
                                } else {
                                    WeatherItemView(model: model)
                                }
                            }
                        }
                    }.refreshable {
                        if !connectivity.checkConnection() {
                          return
                        }
                        self.viewModel.getWeather()
                        self.viewModel.getWeatherOnFavCities()
                    }
                }

            }
            .navigationTitle("app_name")
            .toolbar {
                ToolbarItem(placement: .confirmationAction) {

                    NavigationLink(destination: CityInputView(refreshFavoriteCities: $refreshFavoriteCities)
                                    .environmentObject(connectivity),
                                   isActive: $isActive) {
                        Button(action: {
                            if !connectivity.checkConnection() {
                                showingAlertError.toggle()
                            } else {
                                isActive.toggle()
                            }
                        }) {
                            Text("more")
                        }.accessibilityIdentifier("ToolBarMore")
                    }
                }
            }
        }
        .alert(isPresented: $showingAlertError) {
            Alert(
                title: Text("connection"),
                message: Text("lost_connection"),
                dismissButton: .default(Text("ok"))
            )
        }
        .onChange(of: refreshFavoriteCities) { newValue in
            if newValue {
                self.viewModel.getWeatherOnFavCities()
                self.refreshFavoriteCities = false
            }
        }.navigationViewStyle(StackNavigationViewStyle())
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        MainView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
    }
}
