//
//  CityInputView.swift
//  Weather
//
//  Created by Cristhian Antonio Aguirre Corea on 6/5/22.
//

import SwiftUI
import CoreLocation
import MapKit
import BottomSheetSwiftUI

struct CityInputView: View {

    @Environment(\.presentationMode) private var presentation: Binding<PresentationMode>

    @EnvironmentObject var connectivity: Connectivity

    @ObservedObject private var viewModel = CityInputViewModel()

    @Binding var refreshFavoriteCities: Bool

    @State var bottomSheetPosition: BottomSheetPosition = .hidden
    @State private var selectCity: GeoCodeElement?
    @State private var cityEnabled = true
    @State private var mapEnabled = false
    @State private var showingAlertError = false
    @State private var showingInternetAlertError = false
    @State private var showingConfirmation = false
    @State private var cityNameTxt: String = ""
    @State private var longitudeTxt: String = ""
    @State private var latitudeTxt: String = ""
    @State private var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: MapDefault
                                            .latitude,
                               longitude: MapDefault
                                            .longitude),
                              span: MKCoordinateSpan(latitudeDelta: MapDefault.delta,
                                                       longitudeDelta: MapDefault.delta))

    var headerContentBottomSheet: some View {
        VStack(alignment: .leading) {
            Text("results")
                   .font(.title)
                   .bold()

            Text("info_results")
                   .font(.subheadline)
                   .foregroundColor(.secondary)

           }
    }

    var body: some View {

        VStack {

            VStack {
                Form {
                    Section(header: Text("search_criteria")) {
                        Toggle(isOn: $cityEnabled.animation()) {
                            Text(cityEnabled ? "by_city" : "by_geocode")
                        }.accessibilityIdentifier("Criteria")
                        .onChange(of: cityEnabled) { _ in
                            viewModel.geoCodeInfo = nil

                            viewModel.seeResults = false
                            mapEnabled = false

                            cityNameTxt = ""
                            latitudeTxt = ""
                            longitudeTxt = ""
                        }
                    }

                    if cityEnabled {
                        Section(header: Text("by_city")) {
                            TextField("seach_city", text: $cityNameTxt)
                                .showClearButton($cityNameTxt)
                                .submitLabel(.done)
                                .accessibilityIdentifier("CityTextField")
                                .onSubmit {
                                    if !connectivity.checkConnection() {
                                        showingInternetAlertError.toggle()
                                    } else {
                                        self.viewModel.doneCityTextField(query: cityNameTxt)
                                    }
                                }.alert(isPresented: $showingInternetAlertError) {
                                    Alert(
                                        title: Text("connection"),
                                        message: Text("lost_connection_input"),
                                        dismissButton: .default(Text("ok"))
                                    )
                                }

                            if viewModel.isLoading {
                                ProgressView()
                            } else {
                                if viewModel.seeResults {
                                    HStack {
                                        Image(systemName: "info.circle")
                                            .resizable()
                                            .aspectRatio(contentMode: ContentMode.fill)
                                            .frame(width: 20, height: 20)
                                            .foregroundColor(.gray)

                                        Text("see_results")
                                            .foregroundColor(.blue)
                                            .padding(2)
                                    }.onTapGesture {
                                        self.bottomSheetPosition = .middle
                                    }.accessibilityIdentifier("SeeResults")
                                }
                            }
                        }
                    }

                    if !cityEnabled {
                        Section(header: Text("by_geocode")) {

                            TextField("latitude", text: $latitudeTxt)
                                .showClearButton($latitudeTxt)
                                .accessibilityIdentifier("LatTextField")
                                .onChange(of: mapEnabled) { newValue in
                                    if !newValue {
                                        latitudeTxt = ""
                                    }
                                }

                            TextField("longitude", text: $longitudeTxt)
                                .showClearButton($longitudeTxt)
                                .accessibilityIdentifier("LonTextField")
                                .onChange(of: mapEnabled) { newValue in
                                    if !newValue {
                                        longitudeTxt = ""
                                    }
                                }

                            Toggle(isOn: $mapEnabled.animation()) {
                                Text("use_map")
                            }.accessibilityIdentifier("MapEnabled")
                            .onChange(of: mapEnabled) { _ in
                                    viewModel.geoCodeInfo = nil
                                    viewModel.seeResults = false
                            }

                            if viewModel.isLoading {
                                ProgressView()
                            } else {
                                if viewModel.seeResults {
                                    HStack {
                                        Image(systemName: "info.circle")
                                            .resizable()
                                            .aspectRatio(contentMode: ContentMode.fill)
                                            .frame(width: 20, height: 20)
                                            .foregroundColor(.gray)

                                        Text("see_results")
                                            .foregroundColor(.blue)
                                            .padding(2)
                                    }.onTapGesture {
                                        self.bottomSheetPosition = .middle
                                    }.accessibilityIdentifier("SeeResultsGEO")
                                }
                            }

                            if mapEnabled {
                                Map(coordinateRegion: $region,
                                interactionModes: .all,
                                showsUserLocation: true)
                                    .accessibilityIdentifier("MapView")
                                    .frame(height: 300)
                                    .ignoresSafeArea()
                                    .onTapGesture {
                                        latitudeTxt = region
                                                        .center
                                                        .latitude.toString(format: "%.4f")
                                        longitudeTxt = region
                                                        .center
                                                        .longitude.toString(format: "%.4f")
                                    }
                            }

                            Button {

                                if !connectivity.checkConnection() {
                                    showingInternetAlertError.toggle()
                                } else {
                                    selectCity = nil
                                    viewModel.searchByGeoCode(lat: latitudeTxt,
                                                              long: longitudeTxt)
                                }
                            } label: {
                                Text("search_by_geocode")
                            }.accessibilityIdentifier("SearchByGeocode")
                            .disabled(latitudeTxt.isEmpty || longitudeTxt.isEmpty)
                            .alert(isPresented: $showingInternetAlertError) {
                                Alert(
                                    title: Text("connection"),
                                    message: Text("lost_connection_input"),
                                    dismissButton: .default(Text("ok"))
                                )
                            }
                            .frame(minWidth: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/,
                                    idealWidth: 0,
                                    maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/,
                                    minHeight: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/,
                                    idealHeight: 0,
                                    maxHeight: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/,
                                    alignment: .center)

                        }
                    }

                    Button {
                        if !connectivity.checkConnection() {
                            showingInternetAlertError.toggle()
                        } else {
                            self.selectCity == nil ? showingAlertError.toggle() : showingConfirmation.toggle()
                        }
                    } label: {
                        Text("city_input")
                    }.accessibilityIdentifier("AddCity")
                    .disabled(viewModel.geoCodeInfo == nil)
                    .alert(isPresented: $showingAlertError) {
                        Alert(
                            title: Text("error"),
                            message: Text("info_error"),
                            dismissButton: .default(Text("Ok"))
                        )
                    }
                    .frame(minWidth: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/,
                            idealWidth: 0,
                            maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/,
                            minHeight: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/,
                            idealHeight: 0,
                            maxHeight: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/,
                            alignment: .center)
                }
            }

        }.navigationTitle("city_input")
        .onDisappear(perform: {
            viewModel.seeResults  = false
            viewModel.geoCodeInfo = nil
        })
        .alert(isPresented: $showingConfirmation) {
            Alert(
                title: Text("confirmation_title"),
                message: Text("body_confirmation \(selectCity?.name ?? "")"),
                primaryButton: .default(Text("ok")) {
                    viewModel.saveGeoCode(with: selectCity)
                    refreshFavoriteCities.toggle()
                    presentation.wrappedValue.dismiss()
                },
                secondaryButton: .destructive(Text("cancel"))
            )
        }
        .bottomSheet(bottomSheetPosition: self.$bottomSheetPosition, options: [
                .noDragIndicator,
                .allowContentDrag, .showCloseButton(),
                .swipeToDismiss,
                .tapToDismiss
        ], headerContent: { headerContentBottomSheet }) {
            ResultsByGeocodeView(geoCodeInfo: viewModel.geoCodeInfo,
                                 selectCity: $selectCity)
        }
    }
}

struct CityInputView_Previews: PreviewProvider {
    static var previews: some View {
        CityInputView(refreshFavoriteCities: .constant(true))
    }
}
