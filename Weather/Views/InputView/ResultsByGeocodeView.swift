//
//  ResultsByGeocodeView.swift
//  Weather
//
//  Created by Cristhian Antonio Aguirre Corea on 8/5/22.
//

import SwiftUI

struct ResultsByGeocodeView: View {

    var geoCodeInfo: GeoCode?
    @Binding var selectCity: GeoCodeElement?

    var body: some View {
        if geoCodeInfo?.isEmpty ?? true {
            Text("empty_results")
                .foregroundColor(.gray)
                .font(.title3)
        } else {
            List {
                ForEach(geoCodeInfo ?? [], id: \.id) { model in
                    SelectionCell(model: model,
                                  selectCity: $selectCity)
                        .onAppear {
                                // SELECCIONAR SIEMPRE LA PRIMERA CIUDAD POR DEFECTO
                            self.selectCity = self.geoCodeInfo?.first
                        }
                }
            }
        }
    }
}

struct SelectionCell: View {

    let model: GeoCodeElement
    @Binding var selectCity: GeoCodeElement?

    var body: some View {
        HStack {
            ResultsByCityNameView(geoCodeInfo: model)
            Spacer()
            if model == selectCity {
                Image(systemName: "checkmark")
                    .foregroundColor(.accentColor)
            }
        }   .onTapGesture {
                self.selectCity = self.model
            }
    }
}

struct ResultsByGeocodeView_Previews: PreviewProvider {
    static var previews: some View {
        ResultsByGeocodeView(geoCodeInfo: [
            GeoCodeElement(name: "London",
                           lat: 51.5085,
                           lon: -0.1257,
                           country: "GB",
                           state: nil),
            GeoCodeElement(name: "London",
                           lat: 42.9834,
                           lon: -81.233,
                           country: "CA",
                           state: nil),
            GeoCodeElement(name: "London",
                           lat: 37.129,
                           lon: -84.0833,
                           country: "US",
                           state: "KY")
        ], selectCity: .constant(nil))
    }
}
