//
//  ResultsByCityNameView.swift
//  Weather
//
//  Created by Cristhian Antonio Aguirre Corea on 7/5/22.
//

import SwiftUI

struct ResultsByCityNameView: View {

    var geoCodeInfo: GeoCodeElement?

    init(geoCodeInfo: GeoCodeElement?) {
        self.geoCodeInfo = geoCodeInfo
    }

    var body: some View {
        VStack {

            ZStack {

                RoundedRectangle(cornerRadius: 25, style: .continuous)
                    .fill(.white)

                VStack {
                    Text("\(self.geoCodeInfo?.name ?? "unknown"), \(self.geoCodeInfo?.country ?? "unknown")")
                        .font(.title)
                        .foregroundColor(.black)
                        .padding(2)

                    HStack {
                        Text("Lat: \(self.geoCodeInfo?.lat.toString(format: "%.4f") ?? "unknown")")
                            .font(.body)
                            .foregroundColor(.gray)

                        Text("Long: \(self.geoCodeInfo?.lon.toString(format: "%.4f") ?? "unknown")")
                            .font(.body)
                            .foregroundColor(.gray)
                    }.padding(2)

                }
                .padding()
                .multilineTextAlignment(.center)
            }
        }
    }
}

struct ResultsByCityNameView_Previews: PreviewProvider {
    static var previews: some View {
        ResultsByCityNameView(geoCodeInfo: nil)
    }
}
