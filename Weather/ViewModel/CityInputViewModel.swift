//
//  CityInputViewModel.swift
//  Weather
//
//  Created by Cristhian Antonio Aguirre Corea on 6/5/22.
//

import SwiftUI

class CityInputViewModel: NSObject, ObservableObject {

    @ObservedObject private var httpManager = HttpManager()

    @Published var isLoading: Bool = false
    @Published var geoCodeInfo: GeoCode?
    @Published var disabled = true
    @Published var seeResults = false

    func doneCityTextField(query: String) {
        getCity(by: query.lowercased())
    }

    func searchByGeoCode(lat: String, long: String) {
        getCityByGeoCode(lat: lat, long: long)
    }

    func saveGeoCode(with geoCodeElement: GeoCodeElement?) {
        let context = PersistenceController.shared.container.viewContext

        guard let geoCodeElement = geoCodeElement else {
            print("ciudad seleccionada es nulla")
            return
        }

        let geoCodeDB = TbGeoCode(context: context)

        geoCodeDB.id = UUID.init(uuidString: geoCodeElement.id)
        geoCodeDB.name = geoCodeElement.name
        geoCodeDB.lat = geoCodeElement.lat
        geoCodeDB.lon = geoCodeElement.lon
        geoCodeDB.country = geoCodeElement.country
        geoCodeDB.state = geoCodeElement.state

        do {
            try context.save()
        } catch let error {
            print(error.localizedDescription)
        }

    }

    private func getCityByGeoCode(lat: String, long: String) {
        isLoading.toggle()
        httpManager.request(target: .reverseGeoCoding(lat: lat, lon: long),
                            success: proccessGeoCodeResponse,
                            failure: proccessWeatherError)
    }

    private func getCity(by query: String) {
        isLoading.toggle()
        httpManager.request(target: .geoCoding(query: query),
                            success: proccessGeoCodeResponse,
                            failure: proccessWeatherError)
    }

    private func proccessGeoCodeResponse(model: GeoCode?) {
        guard let model = model else {
            print("WeatherInfo response is nil")
            return
        }

        isLoading.toggle()
        self.geoCodeInfo = model
        self.disabled = model.isEmpty
        if !seeResults {
            self.seeResults.toggle()
        }

    }

    private func proccessWeatherError(error: Error?) {
        isLoading.toggle()
        guard let weatherError = error as? WeatherError else {
            print(error?.localizedDescription ?? "")
            return
        }
        print("Cod: \(weatherError.cod)\tMessage: \(weatherError.message)")
    }

}
