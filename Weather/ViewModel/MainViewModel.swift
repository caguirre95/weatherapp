//
//  MainViewModel.swift
//  Weather
//
//  Created by Cristhian Antonio Aguirre Corea on 5/5/22.
//

import SwiftUI
import Combine
import CoreData
import Alamofire

class MainViewModel: NSObject, ObservableObject {

    @ObservedObject private var locationManager = LocationManager()
    @ObservedObject private var httpManager = HttpManager()

    @Published var isLoading = false

    private var cancellables: [AnyCancellable] = []
    var userLocation: UserLocation?

    override init() {
        super.init()

        locationManager.$lastKnownLocation
            .receive(on: RunLoop.main)
            .sink { [weak self] lastLocation in
                if let coordinate = lastLocation?.coordinate {
                    self?.userLocation = UserLocation(latitude: String(coordinate.latitude),
                                        longitude: String(coordinate.longitude))
                    self?.getWeather()
                }
            }
            .store(in: &cancellables)
    }

    func getWeather() {
        let deviceLang = DeviceUtils.shared.getDeviceLanguage() ?? "us"
        if let userLocation = userLocation {
            isLoading.toggle()
            httpManager.request(target: .weather(lat: userLocation.latitude,
                                                 lon: userLocation.longitude,
                                                 lang: deviceLang),
                                success: proccessWeatherResponse,
                                failure: proccessWeatherError)
        }
    }

    private func proccessWeatherResponse(model: WeatherInfo?) {
        guard let model = model else {
            print("WeatherInfo response is nil")
            return
        }

        isLoading.toggle()
        self.saveOrUpdateWeatherInfo(weatherInfoAPI: model)
    }

    private func proccessWeatherError(error: Error?) {
        isLoading.toggle()
        guard let weatherError = error as? WeatherError else {
            print(error?.localizedDescription ?? "")
            return
        }
        print("Cod: \(weatherError.cod)\tMessage: \(weatherError.message)")
    }

}

// MARK: Core Data functions
extension MainViewModel {

     func getWeatherOnFavCities() {
        let context = PersistenceController.shared.container.viewContext

        let requestGeoCode = TbGeoCode.fetchRequest()

        do {

            let fetchGeoCode = try context.fetch(requestGeoCode)

            if fetchGeoCode.isEmpty {
                print("No existe informacion de geocode a consutlar")
                return
            }

            let deviceLang = DeviceUtils.shared.getDeviceLanguage() ?? "us"

            fetchGeoCode.forEach { geoCode in

                httpManager.request(target: .weather(lat: geoCode.lat.toString(),
                                                     lon: geoCode.lon.toString(),
                                                     lang: deviceLang),
                                    success: proccessWeatherByGeoCodeResponse,
                                    failure: proccessWeatherError)

            }

        } catch let error {
            print(error.localizedDescription)
        }
    }

    private func proccessWeatherByGeoCodeResponse(model: WeatherInfo?) {
        guard let model = model else {
            print("WeatherInfo response is nil")
            return
        }

        let context = PersistenceController.shared.container.viewContext
        do {
            try self.saveCityDB(weatherInfoAPI: model,
                                isFavorite: true,
                                context: context)

        } catch let error {
            print(error.localizedDescription)
        }
    }

    private func hasSavedGeoCodeWeather(by geoCodeid: Int) -> Bool {
        let context = PersistenceController.shared.container.viewContext

        let requestWeatherInfo = TbWeatherInfo.fetchRequest()

        do {

            let fetchWeathersInfo = try context.fetch(requestWeatherInfo)
            return !fetchWeathersInfo.filter({$0.id == geoCodeid}).isEmpty
        } catch let error {
            print(error.localizedDescription)
            return false
        }
    }

    private func saveOrUpdateWeatherInfo(weatherInfoAPI: WeatherInfo) {
        let context = PersistenceController.shared.container.viewContext

        let requestWeatherInfo = TbWeatherInfo.fetchRequest()

        do {

            let fetchWeathersInfo = try context.fetch(requestWeatherInfo)

            if fetchWeathersInfo.isEmpty {
                try saveCityDB(weatherInfoAPI: weatherInfoAPI,
                                  context: context)
                return
            }

            guard let currentCityWeatherDB = fetchWeathersInfo.first else {
                print("Ocurrio un problema en Core Data {MainViewModel}")
                return
            }

            try updateCurrentCityDB(weatherInfoAPI: weatherInfoAPI,
                                currentCityWeatherDB: currentCityWeatherDB,
                                context: context)

        } catch let error {
            print(error.localizedDescription)
        }
    }

    private func saveCityDB(weatherInfoAPI: WeatherInfo,
                            isFavorite: Bool = false,
                            context: NSManagedObjectContext) throws {

        // VALIDAR SI EXISTE LA CIUDAD FAVORITA PARA NO VOLVER A INSERTARLA
        if isFavorite && hasSavedGeoCodeWeather(by: weatherInfoAPI.id) {
            print("Ya se encuentra guarda la ciudad favorita")
            return
        }
        let sysDB = TbSys(context: context)
        let mainDB = TbMain(context: context)
        let weatherDB = TbWeather(context: context)
        let weatherInfoDB = TbWeatherInfo(context: context)

        // SYS INFO
        sysDB.id = UUID()
        sysDB.country = weatherInfoAPI.sys.country

        // MAIN DB
        mainDB.id = UUID()
        mainDB.temp = weatherInfoAPI.main.temp
        mainDB.tempMin = weatherInfoAPI.main.tempMin ?? 0.0
        mainDB.tempMax = weatherInfoAPI.main.tempMax ?? 0.0

        // WEATHER INFO
        weatherDB.id = Int32(weatherInfoAPI.weathers.first?.id ?? 0)
        weatherDB.main = weatherInfoAPI.weathers.first?.main ?? ""
        weatherDB.weatherDescription = weatherInfoAPI.weathers.first?.weatherDescription ?? ""
        weatherDB.icon = weatherInfoAPI.weathers.first?.icon ?? ""

        // Weather INFO
        weatherInfoDB.id =  Int32(weatherInfoAPI.id)
        weatherInfoDB.name = weatherInfoAPI.name
        weatherInfoDB.isFavorite = isFavorite
        weatherInfoDB.sys  = sysDB
        weatherInfoDB.main = mainDB
        weatherInfoDB.weather = weatherDB

        try context.save()
    }

    private func updateCurrentCityDB(weatherInfoAPI: WeatherInfo,
                                     currentCityWeatherDB: TbWeatherInfo,
                                     context: NSManagedObjectContext) throws {
        // SYS INFO
        currentCityWeatherDB.sys?.setValue(UUID(), forKey: "id")
        currentCityWeatherDB.sys?.setValue(weatherInfoAPI.sys.country, forKey: "country")

       // MAIN DB
        currentCityWeatherDB.main?.setValue(UUID(), forKey: "id")
        currentCityWeatherDB.main?.setValue(weatherInfoAPI.main.temp, forKey: "temp")
        currentCityWeatherDB.main?.setValue(weatherInfoAPI.main.tempMin ?? 0.0, forKey: "tempMin")
        currentCityWeatherDB.main?.setValue(weatherInfoAPI.main.tempMax ?? 0.0, forKey: "tempMax")

       // WEATHER INFO

        currentCityWeatherDB.weather?.setValue(Int32(weatherInfoAPI.weathers.first?.id ?? 0),
                                               forKey: "id")
        currentCityWeatherDB.weather?.setValue(weatherInfoAPI.weathers.first?.main ?? "",
                                               forKey: "main")
        currentCityWeatherDB.weather?.setValue(weatherInfoAPI.weathers.first?.weatherDescription ?? "",
                                               forKey: "weatherDescription")
        currentCityWeatherDB.weather?.setValue(weatherInfoAPI.weathers.first?.icon ?? "",
                                               forKey: "icon")

        currentCityWeatherDB.setValue(weatherInfoAPI.name, forKey: "name")
        currentCityWeatherDB.setValue(currentCityWeatherDB.sys, forKey: "sys")
        currentCityWeatherDB.setValue(currentCityWeatherDB.main, forKey: "main")
        currentCityWeatherDB.setValue(currentCityWeatherDB.weather, forKey: "weather")

        try context.save()
    }

}
