//
//  LocalStoreDefault.swift
//  Weather
//
//  Created by Cristhian Antonio Aguirre Corea on 8/5/22.
//

import Foundation

final class LocalStoreDefault: ObservableObject {

    @Published var defaults: UserDefaults

    init(defaults: UserDefaults = .standard) {
        self.defaults = defaults
    }

    var isFirstTimeToSaveInBD: Bool? {
        get {
            defaults.bool(forKey: UserDefaults.Keys.localStoreDefault.rawValue)
        }

        set {
            defaults.set(newValue, forKey: UserDefaults.Keys.localStoreDefault.rawValue)
        }
    }
}

extension UserDefaults {

    enum Keys: String, CaseIterable {
        case localStoreDefault = "app.view.auth.isPolicyAccepted"
    }

    func reset() {
        Keys.allCases.forEach { removeObject(forKey: $0.rawValue) }
    }
}
