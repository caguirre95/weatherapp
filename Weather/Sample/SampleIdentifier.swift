//
//  SampleIdentifier.swift
//  Weather
//
//  Created by Cristhian Antonio Aguirre Corea on 7/5/22.
//

import Foundation

enum SampleIdentifier: String {
    // RESPONSES
    case weatherSampleData          = "WeatherSampleData"
    case geoCodingSampleData        = "GeoCodingSampleData"
    case emptyGeoCodingSuccessData  = "EmptyGeoCodingSuccessData"

    // ERRROS
    case weatherErrorSampleData     = "WeatherErrorSampleData"
}
