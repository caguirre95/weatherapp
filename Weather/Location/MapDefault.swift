//
//  MapDefault.swift
//  Weather
//
//  Created by Cristhian Antonio Aguirre Corea on 8/5/22.
//

import Foundation

enum MapDefault {
    static let latitude  = 12.145991
    static let longitude = -86.274666
    static let delta  = 0.5
}
