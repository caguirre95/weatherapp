//
//  WeatherInfo.swift
//  Weather
//
//  Created by Cristhian Antonio Aguirre Corea on 5/5/22.
//

import Foundation

struct WeatherInfo: Codable, Identifiable {

    var id: Int
    let weathers: [Weather]
    let sys: Sys
    let main: Main
    let name: String

    enum CodingKeys: String, CodingKey {
        case id
        case weathers = "weather"
        case sys
        case main
        case name
    }
}

// MARK: - Weather
struct Weather: Codable {
    let id: Int
    let main, weatherDescription, icon: String

    enum CodingKeys: String, CodingKey {
        case id, main
        case weatherDescription = "description"
        case icon
    }
}

struct Sys: Codable {
    let country: String
}

struct Main: Codable {
    let temp: Double
    let tempMin: Double?
    let tempMax: Double?

    enum CodingKeys: String, CodingKey {
        case temp
        case tempMin = "temp_min"
        case tempMax = "temp_max"
    }
}
