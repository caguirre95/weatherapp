//
//  GeoCodeElement.swift
//  Weather
//
//  Created by Cristhian Antonio Aguirre Corea on 7/5/22.
//

import Foundation

typealias GeoCode = [GeoCodeElement]

struct GeoCodeElement: Codable, Hashable {
    let id: String = UUID().uuidString
    let name: String
    let lat, lon: Double
    let country: String
    let state: String?

    enum CodingKeys: String, CodingKey {
        case name
        case lat, lon, country, state
    }
}
