//
//  UserLocation.swift
//  Weather
//
//  Created by Cristhian Antonio Aguirre Corea on 5/5/22.
//

import Foundation

struct UserLocation: Equatable {
    let identifier: UUID = UUID()
    var latitude: String
    var longitude: String
}
