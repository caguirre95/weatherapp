//
//  WeatherError.swift
//  Weather
//
//  Created by Cristhian Antonio Aguirre Corea on 6/5/22.
//

import Foundation

enum WeatherLocalErrorCodes: Int {
    case nullable = 1000
    case catcher  = 1001
}

enum WeatherLocalError: Error {
    case nullable(code: Int, message: String)
    case catcher(code: Int, message: String)
}

struct WeatherError: Codable, Error, Equatable {
    let cod: String
    let message: String
}
