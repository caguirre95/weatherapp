//
//  WeatherUITests.swift
//  WeatherUITests
//
//  Created by Cristhian Antonio Aguirre Corea on 5/5/22.
//

import XCTest

class WeatherUITests: XCTestCase {

    private var app: XCUIApplication!
    
    let sleepTime: UInt32 = 5 //expected 5s
    
    override func setUp() {
        super.setUp()
        
        self.app = XCUIApplication()
        self.app.launch()
        sleep(1)
    }
    
    func testFlowSearcByName()  {
        let toolBarButton = self.app.buttons["ToolBarMore"]
        toolBarButton.tap()
        
        let cityTextField = self.app.textFields["CityTextField"]
        cityTextField.tap()
        cityTextField.typeText("Milan")
        
        //Busqueda de ciudad por nombre
        let deviceLang = DeviceUtils.shared.getDeviceLanguage()
        
        var keyboardDone = app.keyboards.buttons["done"]
        // Solo se soporta 2 lenguajes
        if deviceLang == "es" {
            keyboardDone = app.keyboards.buttons["aceptar"]
        }
        
        keyboardDone.tap()
        
        //Wait for
        sleep(sleepTime)
        
        let seeResultsButton = self.app.staticTexts["SeeResults"]
        seeResultsButton.tap()
        
        //DISMISS BOTTOMSHEET
        app.tap()
        
        let addCityButton = self.app.buttons["AddCity"]
        addCityButton.tap()
    }
    
    func testFlowSearcByGeoCodeWithMap()  {
        let toolBarButton = self.app.buttons["ToolBarMore"]
        toolBarButton.tap()
        
        let criteriaToggle = self.app.switches["Criteria"]
        criteriaToggle.tap()
        
        let mapToggle = self.app.switches["MapEnabled"]
        mapToggle.tap()
        
        let mapView = self.app.otherElements.matching(identifier: "MapView")
        mapView.element.tap()
        
       searchByGeoCodeSteps()
    }
    
    func testFlowSearchByGeoCodeWithoutMap()  {
        let toolBarButton = self.app.buttons["ToolBarMore"]
        toolBarButton.tap()
        
        let criteriaToggle = self.app.switches["Criteria"]
        criteriaToggle.tap()
        
        let latTextField = self.app.textFields["LatTextField"]
        latTextField.tap()
        latTextField.typeText("21.1617854")
        
        let lonTextField = self.app.textFields["LonTextField"]
        lonTextField.tap()
        lonTextField.typeText("-86.8510468")
        
        searchByGeoCodeSteps()
        
    }
    
    private func searchByGeoCodeSteps() {
        let searchByGeoCodeButton = self.app.buttons["SearchByGeocode"]
        searchByGeoCodeButton.tap()
        
        //Wait for
        sleep(sleepTime)
        
        let seeResultsButton = self.app.staticTexts["SeeResultsGEO"]
        seeResultsButton.tap()
        
        //DISMISS BOTTOMSHEET
        app.tap()
        
        let addCityButton = self.app.buttons["AddCity"]
        addCityButton.tap()
    }

}
