
# Weather
Aplicación que muestra el clima de la ciudad actual y agrega ciudades en una lista de favoritas. Según el requerimiento solo se debe de mostrar el clima de la ubicación actual del usuario no de las ciudades favoritas aún así por valor agregado se muestra el clima de las ciudades ingresadas posteriormente.

Por otro lado la ubicación del usuario se captura al iniciar la app y sola esa única vez ( haciendo pull refresh a la lista solo se refresca la información del clima no la ubicación del usuario), en los requerimientos no se indica que se debe  actualizar el clima en un intervalo de tiempo o movimiento del usuario aún así se hizo una integración, si el usuario se movia 2500mts se consultaba nuevamente la API para actualizar los datos del clima en la primera fila, aunque luego se retiro la implementación al ejecutar los UI Tests debido a que se encontro un problema y demandaba tiempo de desarrollo por lo tanto decidí volver al código original para terminar los requerimientos (Esto se puede verificar en el historial de Git).

## Capturas
### iPhone 13 Pro Max Simulador

<img src= "Capturas/Home.png" alt="Inicio" width="200" height="420">
<img src= "Capturas/AddCityByName.png" alt="Agregar ciudad por nombre" width="200" height="420">
<img src= "Capturas/AddCityByGeocode.png" alt="Agregar ciudad por coordenadas" width="200" height="420">
<img src= "Capturas/GeoCodeResults.png" alt="Resultados de la búsqueda" width="200" height="420">
<img src= "Capturas/AddCityDialog.png" alt="Diálogo de confirmación" width="200" height="400">

## Dispositivos
Lista de dispositivos en lo que fue ejecutada la aplicación

 Nombre | Físico | Simulador |
| :---         |     :---:      |          :---: |
| iPhone 13 Pro Max  |     |   X   |
| iPhone 12 Pro Max     | X       |      |

## Git
Se utilizo Gitflow como metodología de versionamiento de código
- features para construir el producto
- develop,  alberga las iteraciones del producto
- release, candidata a producción aqui se realizan las pruebas (en este caso la revisión por parte del personal de Nica Source)
- main/master, código productivo

NOTA: Se solicitaron Merge Request para aceptar las integraciones en develop (aunque se asignaba a mi 😅)

## Características

- [x] Obtener el clima de la ciudad actual según ubicación del usuario
- [x] Agregar ciudad por nombre o por coordenadas
- [x] Modo sin conexión

## Requerimientos

- iOS 15.0+
- Xcode 13.2.1
- Swift 5.5.2

## SwiftLinter
Para formateo de código utilice SwiftLinter (se agrego via CocoaPods), en caso de tener problema con el pod de SwiftLint puede utilizar:

##### Vía Homebrew
```
brew install swiftlint
```
##### Vía Mint
```
mint install realm/SwiftLint
```

## Persistencia
Base de datos: **Core Data**

## Instalación

#### CocoaPods
Se utilizo CocoaPods para las dependencias utilizadas en el proyecto para instalarlas debe ubicarse en la raíz del proyecto y ejecutar el siguiente comando:

```
pod install
```
Si utiliza M1 y tiene problema para ejecutar el comando favor realizar los siguientes pasos:

```
sudo gem uninstall cocoapods
brew install cocoapods
```

Si aún persiste el error favor usar esta manera y luego `pod install`:
```
sudo arch -x86_64 gem install ffi
arch -x86_64 pod install
```

**IMPORTANTE:** Este paso debe de hacerlo antes de ejecutar el proyecto debido a que no se versiona el proyecto `Pods` generado, por lo tanto al abrir xcworkspace arrojará error.

## Nota

Proyecto por Cristhian Antonio Aguirre Corea  – cristhianaguirre75@gmail.com

Entrega de la prueba finalizada, agradezco favor contactarme en cualquiera de los casos (positivos/negativos) de la plaza, agradezco su feedback para seguir mejorando y gracias por la oportunidad de demostrar mis habilidades.

Para cualquier contacto tienen mi información personal.

Estamos en contacto!

